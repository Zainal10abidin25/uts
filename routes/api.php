<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('list-barang', 'API\barangController@index');
    Route::post('add-barang', 'API\barangController@store');
    Route::get('find-barang/{id}', 'API\barangController@show');
    Route::post('update-barang/{id}', 'API\barangController@update');
    Route::post('delete-barang/{id}', 'API\barangController@destroy');

    Route::get('list-kurir', 'API\kurirController@index');
    Route::post('add-kurir', 'API\kurirController@store');
    Route::get('find-kurir/{id}', 'API\kurirController@show');
    Route::post('update-kurir/{id}', 'API\kurirController@update');
    Route::post('delete-kurir/{id}', 'API\kurirController@destroy');

    Route::get('list-lokasi', 'API\lokasiController@index');
    Route::post('add-lokasi', 'API\lokasiController@store');
    Route::get('find-lokasi/{id}', 'API\lokasiController@show');
    Route::post('update-lokasi/{id}', 'API\lokasiController@update');
    Route::post('delete-lokasi/{id}', 'API\lokasiController@destroy');

    Route::get('list-lokasi', 'API\pengirimanController@index');
    Route::post('add-lokasi', 'API\pengirimanController@store');
    Route::get('find-lokasi/{id}', 'API\pengirimanController@show');
    Route::post('update-lokasi/{id}', 'API\pengirimanController@update');
    Route::post('delete-lokasi/{id}', 'API\pengirimanController@destroy');
});



